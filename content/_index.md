---
title: Home 
type: docs
---

We meet Tuesdays 3:15-5:15 pm UTC via Zoom. We also have a Discord discussion channel.

## Timetable
---
| Date | Topic | Speaker | Notes | References |
| ------ | ------ | ------ | ------ | ------ |
| 19.10 | Introduction to derived algebraic geometry | Zachary Gardner | [PDF][Intro_to_DAG] | [[CS19], Appendix to Lecture II], [[CS21], &sect;5.1.4], [[Lur], &sect;1-2], [[Lur17a], &sect;1.1.2 & 1.2.1], [[May99], Ch. 5], [[Toe14], &sect;1-2] |
| 26.10 | Categorified algebraic geometry | Zachary Gardner | [PDF][Categorified_Algebraic_Geometry] | [[Ras18]], [[Toe]] |
| 09.11 | Derived schemes | Zachary Gardner | [PDF][Derived_Schemes] | [[Lur17b], Ch. 6], [[Lur18], &sect;1.3], [[preschema17], Lecture 1] |
| 16.11 | Quasicoherent sheaves | Zachary Gardner | [PDF][Quasicoherent_Sheaves] | [[Kha16], &sect;0.4-5], [[Lur17a], &sect;4.7], [[Lur18], &sect;5.6.5],  [[preschema17], Lecture 1-2] |
| 30.11 | The cotangent complex | Jeroen Hekking | [PDF][The_Cotangent_Complex] | [[dac], Lecture 3], [[Lur]], [[Lur18]] |
| 07.12 | Derived blow-ups | Jeroen Hekking | [PDF][Derived_Blowups] | [[Lur]], [[Lur18]], [[Vez10]] |

---

## Learning Goals
---
The following are things we want to make sure we cover (at least in some capacity).

- Derived intersection theory
- Derived deformation theory
- Koszul duality
- Hoschschild-Konstant-Rosenberg (HKR) theorem

The following are things we would (maybe) like to work towards (though we may not get to them).

- Virtual fundamental classes
- Derived algebraic cobordism
- Gromov-Witten theory
- Topological modular forms

---

## Philosophy
---
Here are some aspects of our philosophy.

- We want to think in terms of homotopy types and so avoid things like model structures as much as possible.
- We want to avoid or ignore set-theoretic issues whenever possible.
- We want to "play around" with the mathematics, focusing mainly on examples and questions rather than formalism.

---

## References
---
Derived Algebraic Geometry

- Khan: [Motivic homotopy theory in derived algebraic geometry][Kha16] (2016)
- Lurie: [Derived Algebraic Geometry][Lur] (expansion of PhD thesis)
- Lurie: [Derived Algebraic Geometry X: Formal Moduli Problems][Lur11] (2011)
- Lurie: [Higher Topos Theory][Lur17b] (2017)
- Lurie: [Spectral Algebraic Geometry][Lur18] (2018)
- Toen: [Derived Algebraic Geometry][Toe14] (2014)
- Vezzosi: [A note on the cotangent complex in derived algebraic geometry][Vez10] (2010)

Infinity Categories

- Lurie: [Higher Algebra][Lur17a] (2017)
- Raskin: [Day 1, Tutorial 1. Higher Category Theory][Ras14] (2014)

Homotopy Theory

- Goerss and Jardine: [Simplicial Homotopy Theory][GJ09] (2009)
- May: [A Concise Course in Algebraic Topology][May99] (1999)

Categorified Algebraic Geometry and Stacks

- Demazure and Gabriel: [Groupes algebriques][DG70] (1970)
- Laumon and Moret-Bailly: [Champs algebriques][LM00] (2000)
- Raskin: [M392C Notes: Algebraic Geometry][Ras18] (2018)
- Strickland: [Formal schemes and formal groups][Str00] (2000)
- Toen: [A master course on algebraic stacks][Toe] (2019 archive of English translation)

Miscellaneous

- Cesnavicius and Scholze: [Purity for flat cohomology][CS21] (2021)
- Clausen and Scholze: [Lectures on Condensed Mathematics][CS19] (2019)
- Hebestreit: [Algebraic and Hermitian K-Theory][Heb20] (2020)

---

## Contact
---
Please get in touch if you want to participate or have comments/suggestions:

Zachary: [website][zachssite], [mail](mailto:zachary.gardner@bc.edu) 

Aaron: [website][aaronssite], [mail](mailto:s6aawild@uni-bonn.de)

---

## Other Learning Groups
---
There are many other learning groups out there for DAG and related topics. Here are just a few.

- Fall 2011: [DAG reading group][uchicago]
- Spring 2015: [Derived Algebraic Geometry Seminar][utexas]
- Fall 2017-Spring 2018: [Descent in algebraic K-theory][preschema17]
- Fall 2018: [Seminar on derived algebraic geometry][psafronov]
- Spring 2020: [Algebraic K-theory and Derived algebraic geometry][ktheoryanddag]
- Summer 2020: [DAG Summer Mini-course at UT][mini]
- Summer 2020: [Descent in algebraic K-theory][preschema20]
- Fall 2020: [Columbia-Penn Fall 2020 DAG learning seminar][columbia]
- Fall 2021-Spring 2022: [Oberseminar: Derived algebraic cobordism][dac]

---

[CS19]: https://www.math.uni-bonn.de/people/scholze/Condensed.pdf
[CS21]: https://arxiv.org/abs/1912.10932
[DG70]: https://www.worldcat.org/title/groupes-algebriques/oclc/679040329
[GJ09]: https://www.springer.com/gp/book/9783034601887
[Heb20]: https://florianadler.github.io/AlgebraBonn/KTheory.pdf
[Kha16]: https://www.preschema.com/papers/thesis.pdf
[LM00]: https://link.springer.com/book/10.1007/978-3-540-24899-6
[Lur]: https://people.math.harvard.edu/~lurie/papers/DAG.pdf
[Lur11]: https://people.math.harvard.edu/~lurie/papers/DAG-X.pdf
[Lur17a]: https://people.math.harvard.edu/~lurie/papers/HA.pdf
[Lur17b]: https://arxiv.org/abs/math/0608040
[Lur18]: https://www.math.ias.edu/~lurie/papers/SAG-rootfile.pdf
[May99]: https://press.uchicago.edu/ucp/books/book/chicago/C/bo3777031.html
[Ras14]: https://a42663fe-a-62cb3a1a-s-sites.googlegroups.com/site/geometriclanglands2014/notes/inftycateg.pdf?attachauth=ANoY7cp8NPFapO_BD_GWUUDnaPgHF18uQJTBG2WD0OsPTmDdbSbxvvYiq2QW6T70sXx9kIPtiicXkTW69fFi4uf16xZdw5ZcGSxw-PCvRe3Ky2WmvRrCLVKxxlr8ggI9-GJCoCvKJYB_Y0jTA1Q8lc8s6i1k7sy79vUzu_LL3zZd1bFcjevs6FQQc6f9iIWoU9XcOSXEvFev8NrBzzC2bODUj2q-ylZBR968Q217T9_ZpzclKZxaGMs%3D&attredirects=1
[Ras18]: https://web.ma.utexas.edu/users/a.debray/lecture_notes/m392c_Raskin_AG_notes.pdf
[Str00]: https://arxiv.org/abs/math/0011121
[Toe]: https://web.archive.org/web/20190617183638/http://www-personal.umich.edu/~ruchen/articles/stacks.pdf
[Toe14]: https://arxiv.org/abs/1401.1044
[Vez10]: https://arxiv.org/abs/1008.0601

[columbia]: http://math.columbia.edu/~magenroy/DAG-seminar.html
[dac]: http://www.mathematik.uni-regensburg.de/hoyois/WS22/cobordism/
[ktheoryanddag]: https://ktheoryanddag.wordpress.com/
[mini]: https://www.notion.so/DAG-Summer-Mini-course-at-UT-f0be19b247164c5fb3d2d5e0486cf50b#d933fe20ff9a4f22ba517217f034d31d
[preschema17]: https://www.preschema.com/lecture-notes/kdescent/
[preschema20]: https://www.preschema.com/teaching/2020-ss/kdescent/
[psafronov]: https://sites.google.com/site/psafronov/notes/dag?authuser=0
[uchicago]: http://math.uchicago.edu/~amathew/dag.html
[utexas]: https://web.ma.utexas.edu/users/rhughes/DAGX/DAG.html

[aaronssite]: https://pankratius.gitlab.io
[zachssite]: https://zacharygardner.net

[Intro_to_DAG]: https://github.com/zachary137/math-meta/blob/main/notes/DAG/Intro%20to%20DAG.pdf
[Categorified_Algebraic_Geometry]: https://github.com/zachary137/math-meta/blob/main/notes/DAG/Categorified%20Algebraic%20Geometry.pdf
[Derived_Schemes]: https://github.com/zachary137/math-meta/blob/main/notes/DAG/Derived%20Schemes.pdf
[Quasicoherent_Sheaves]: https://github.com/zachary137/math-meta/blob/main/notes/DAG/Quasicoherent%20Sheaves.pdf
[The_Cotangent_Complex]: https://github.com/zachary137/math-meta/blob/main/notes/DAG/Hekking%20-%20The%20Cotangent%20Complex.pdf
[Derived_Blowups]: https://github.com/zachary137/math-meta/blob/main/notes/DAG/Hekking%20-%20More%20on%20Deformation%20Theory%20and%20Derived%20Blowups.pdf
